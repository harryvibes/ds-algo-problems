class Solution:

    def goodpair(self, A, B):
        
        digit_counter = {}
        
        for i in range(len(A)):
            digit_counter[A[i]] = digit_counter.get(A[i], 0) + 1
        
        for i in range(len(A)):
            if B - A[i] in digit_counter:
                if A[i] != B - A[i]:
                    return 1
                
                elif digit_counter[B - A[i]] > 1:
                    return 1
                
        return 0
